import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.HomePage;
import pages.LoginPage;

public class Test_Poprawnej_Rejestracji_Uzytkownika_1_Test extends SeleniumBaseTest {
    @Test(dataProvider = "getAccounts")
    public void correctRegisterTest(String email, String password) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.goToRegisterLink();
        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        createAccountPage.typeEmail(email);
        createAccountPage.typePassword(password);
        createAccountPage.typeConfirmCorrectPassword(password);
        HomePage homePage = createAccountPage.submitRegisterLink();
        homePage.assertWelcomeElementIsShown(email);
    }

    @DataProvider
    public Object[][] getAccounts() {
        return new Object[][]{
                {randomUsername(), "Abcdef1!"},
                {randomUsername(), "12345678Ab!"},
                {randomUsername(), "Okej123Ab#"},
                {randomUsername(), "12ab(%%B"},
        };
    }

    private String randomUsername() {
        return RandomStringUtils.randomAlphabetic(5) + "@poczta.pl";
    }
}