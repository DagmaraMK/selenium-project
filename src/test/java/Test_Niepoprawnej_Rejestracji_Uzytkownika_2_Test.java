import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class Test_Niepoprawnej_Rejestracji_Uzytkownika_2_Test extends SeleniumBaseTest {
    @Test
    public void incorrectRegisterWithNoConfirmTest() {
        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterLink();
        createAccountPage.registerWithFailure("pikpok@poczta.pl", "Pikpok1!", "Pikpok12!");
        WebElement error = driver.findElement(By.id("ConfirmPassword-error"));
        assertTrue(error.isDisplayed());
        assertEquals(error.getText(), "The password and confirmation password do not match.");
    }

    @Test
    public void incorrectRegisterWithToShortPasswordTest() {
        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterLink();
        createAccountPage.registerWithFailure("pikpok@poczta.pl", "abcde", null);
        WebElement error = driver.findElement(By.id("Password-error"));
        assertTrue(error.isDisplayed());
        assertEquals(error.getText(), "The Password must be at least 6 and at max 100 characters long.");
    }
}