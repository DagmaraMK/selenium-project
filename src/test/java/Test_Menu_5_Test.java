import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_Menu_5_Test extends SeleniumBaseTest {

    @Test
    public void testMenu() {
        new LoginPage(driver)
                .typeEmail(getConfig().getApplicationUser())
                .typePassword(getConfig().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .assertUrl("http://localhost:4444/Projects")
                .assertProcessesHeaderIsShown()
                .goToCharacteristics()
                .assertCharacteristicsHeaderIsShown()
                .assertUrl("http://localhost:4444/Characteristics")
                .goToDashboards()
                .assertDashboardUrl("http://localhost:4444/")
                .assertDemoProjectIsShown();
    }
}