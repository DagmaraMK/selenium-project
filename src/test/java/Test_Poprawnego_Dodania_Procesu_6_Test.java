import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_Poprawnego_Dodania_Procesu_6_Test extends SeleniumBaseTest {

    @Test
    public void addProcessTest() {
        String processName = "Witam";
        new LoginPage(driver)
                .typeEmail(getConfig().getApplicationUser())
                .typePassword(getConfig().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .submitCreate()
                .assertProcess(processName, "", "");
    }
}