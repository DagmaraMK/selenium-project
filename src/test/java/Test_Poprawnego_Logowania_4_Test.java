import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

public class Test_Poprawnego_Logowania_4_Test extends SeleniumBaseTest {
    @Test
    public void correctLoginTest() {
        LoginPage loginPage = new LoginPage(driver);

        loginPage.typeEmail(getConfig().getApplicationUser());
        loginPage.typePassword(getConfig().getApplicationPassword());

        HomePage homePage = loginPage.submitLogin();
        homePage.assertWelcomeElementIsShown(getConfig().getApplicationUser());
    }
}