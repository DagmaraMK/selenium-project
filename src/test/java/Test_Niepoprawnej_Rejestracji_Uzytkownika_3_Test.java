import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class Test_Niepoprawnej_Rejestracji_Uzytkownika_3_Test extends SeleniumBaseTest {
    @Test(dataProvider = "getPasswords")
    public void incorrectRegisterWithToErrorPasswordTest(String password, String message) {
        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterLink();
        createAccountPage.registerWithFailure("pikpok@poczta.onet.pl", password, password);
        List<WebElement> errors = driver.findElements(By.cssSelector(".text-danger>ul>li"));
        boolean errorExists = errors.stream().anyMatch(webElement -> webElement.getText().equals(message));
        assertTrue(errorExists, "No message for: " + message);
    }

    @DataProvider
    private Object[][] getPasswords() {
        return new Object[][]{
                {"abcde1!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Abcde!", "Passwords must have at least one digit ('0'-'9')."},
                {"Abcde1", "Passwords must have at least one non alphanumeric character."}
        };
    }
}