package pages;

import config.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ProcessesPage extends HomePage {

    private String PAGE_URL = new Config().getApplicationUrl() + "Projects";
    private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";


    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(linkText = "Add new process")
    private WebElement addProcessBtn;

    @FindBy(css = ".page-title h3")
    private WebElement pageHeader;


    public CreateProcessPage clickAddProcess() {
        addProcessBtn.click();

        return new CreateProcessPage(driver);
    }

    @FindBy(css = ".x_title>h2")
    WebElement processesTxt;

    public ProcessesPage assertProcessesHeaderIsShown() {
        Assert.assertTrue(processesTxt.isDisplayed());
        Assert.assertEquals(processesTxt.getText(), "Processes");
        return this;
    }

    public ProcessesPage assertUrl(String expUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }

    public ProcessesPage assertProcess(String expName, String expDescription, String expNotes) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expName);

        WebElement processRow = driver.findElement(By.xpath(processXpath));

        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();

        Assert.assertEquals(actDescription, expDescription);
        Assert.assertEquals(actNotes, expNotes);

        return this;
    }
}