package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateAccountPage {

    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(css = "#Password")
    private WebElement passwordTxt;

    @FindBy(css = "#ConfirmPassword")
    private WebElement ConfirmCorrectPasswordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement registerLink;

    public CreateAccountPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public CreateAccountPage typeConfirmCorrectPassword(String ConfirmCorrectPassword) {
        ConfirmCorrectPasswordTxt.clear();
        ConfirmCorrectPasswordTxt.sendKeys(ConfirmCorrectPassword);
        return this;
    }

    public HomePage submitRegisterLink() {
        registerLink.click();
        return new HomePage(driver);
    }

    public void submitRegisterLinkWithFailure() {
        registerLink.click();
    }

    public CreateAccountPage registerWithFailure(String email, String password, String confirmPassword) {
        typeEmail(email);
        typePassword(password);
        if (confirmPassword != null) {
            typeConfirmCorrectPassword(confirmPassword);
        }
        submitRegisterLinkWithFailure();

        return this;
    }
}